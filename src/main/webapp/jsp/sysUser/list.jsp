<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2024-04-23
  Time: 12:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
</head>
<body>
<h1>查询用户列表</h1>
<form action="${pageContext.request.contextPath}/sysUser/list" method="post">
    用户名称:<input type="text" name="realName"/>
    <input type="submit" name="查询"/>
</form>
<c:forEach var="sysUser" items="${queryUserList}">
    <div>
        id=${sysUser.id};
        用户编号=${sysUser.account};
        用户名称=${sysUser.realname}
        用户密码=${sysUser.password};
        用户地址=${sysUser.address};
    </div>
</c:forEach>
</body>
</html>
