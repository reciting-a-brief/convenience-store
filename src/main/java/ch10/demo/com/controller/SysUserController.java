package ch10.demo.com.controller;

import ch10.demo.com.pojo.SysUser;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Date;

/**
 * @Description: TODO
 * @author: 周杰
 * @date: 2024年04月23日11:34
 */
@Controller
@RequestMapping("/sysUser")
/*@Scope("prototype")*/
public class SysUserController {
    private volatile Logger logger = Logger.getLogger(SysUserController.class);
    private volatile ArrayList<SysUser> userList = new ArrayList<>();
    public SysUserController(){
        try {
            userList.add(new SysUser(5L,"zhaojing","赵静","5555555",1
                    ,"13054784445","上海市宝山区",1L,1L
                    ,new Date(),1L,new Date()));
            userList.add(new SysUser(4L,"wanglin","王林","4444444",1
                    ,"18965652364","北京市学院路",1L,1L
                    ,new Date(),1L,new Date()));
            userList.add(new SysUser(1L,"test001","测试用户001","1111111",1
                    ,"13566669998","北京市朝阳区",1L,1L
                    ,new Date(),1L,new Date()));
            userList.add(new SysUser(2L,"zhaoyan","赵燕","2222222",1
                    ,"18678786545","北京海淀区",1L,1L
                    ,new Date(),1L,new Date()));
            userList.add(new SysUser(3L,"test003","测试用户003","1111111",1
                    ,"13121334531","北京市海淀区",1L,1L
                    ,new Date(),1L,new Date()));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    //用于返回查询到的用户数据
    private ArrayList<SysUser> queryUserList = new ArrayList<>();
    @GetMapping("/list")
    public String list(Model model){
        logger.info("当查询条件为空时，查询用户信息"+this);
        queryUserList.clear();
        queryUserList.addAll(userList);
        model.addAttribute("queryUserList",userList);
        return "sysUser/list";
    }
    /**
     * 根据用户名称查询用户列表
     */
    @PostMapping("/list")
    public String list(@RequestParam(value = "realName",required = false) String realName,Model model){
        logger.info("查询条件：【realName】="+realName+",查询用户信息");
        queryUserList.clear();
        if (realName != null && !realName.equals("")){
            for (SysUser user : userList){
                if (user.getRealname().indexOf(realName) != -1) {
                    queryUserList.add(user);
                }
            }
        }else {
            queryUserList.addAll(userList);
        }
        model.addAttribute("queryUserList",queryUserList);
        return "sysUser/list";
    }
}
