package ch10.demo.com.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 药品供货商
 * @TableName t_supplier
 */
public class Supplier implements Serializable {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 供货商编号
     */
    private String supcode;

    /**
     * 供货商名称
     */
    private String supname;

    /**
     * 供货商描述
     */
    private String supdesc;

    /**
     * 供货商联系人
     */
    private String supcontact;

    /**
     * 联系电话
     */
    private String supphone;

    /**
     * 供货商地址
     */
    private String supaddress;

    /**
     * 传真
     */
    private String supfax;

    /**
     * 创建人id
     */
    private Long createduserid;

    /**
     * 创建时间
     */
    private Date createdtime;

    /**
     * 修改时间
     */
    private Date updatedtime;

    /**
     * 修改人id
     */
    private Long updateduserid;

    /**
     * 企业营业执照的存储路径
     */
    private String companylicpicpath;

    /**
     * 组织机构代码证的存储路径
     */
    private String orgcodepicpath;

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 供货商编号
     */
    public String getSupcode() {
        return supcode;
    }

    /**
     * 供货商编号
     */
    public void setSupcode(String supcode) {
        this.supcode = supcode;
    }

    /**
     * 供货商名称
     */
    public String getSupname() {
        return supname;
    }

    /**
     * 供货商名称
     */
    public void setSupname(String supname) {
        this.supname = supname;
    }

    /**
     * 供货商描述
     */
    public String getSupdesc() {
        return supdesc;
    }

    /**
     * 供货商描述
     */
    public void setSupdesc(String supdesc) {
        this.supdesc = supdesc;
    }

    /**
     * 供货商联系人
     */
    public String getSupcontact() {
        return supcontact;
    }

    /**
     * 供货商联系人
     */
    public void setSupcontact(String supcontact) {
        this.supcontact = supcontact;
    }

    /**
     * 联系电话
     */
    public String getSupphone() {
        return supphone;
    }

    /**
     * 联系电话
     */
    public void setSupphone(String supphone) {
        this.supphone = supphone;
    }

    /**
     * 供货商地址
     */
    public String getSupaddress() {
        return supaddress;
    }

    /**
     * 供货商地址
     */
    public void setSupaddress(String supaddress) {
        this.supaddress = supaddress;
    }

    /**
     * 传真
     */
    public String getSupfax() {
        return supfax;
    }

    /**
     * 传真
     */
    public void setSupfax(String supfax) {
        this.supfax = supfax;
    }

    /**
     * 创建人id
     */
    public Long getCreateduserid() {
        return createduserid;
    }

    /**
     * 创建人id
     */
    public void setCreateduserid(Long createduserid) {
        this.createduserid = createduserid;
    }

    /**
     * 创建时间
     */
    public Date getCreatedtime() {
        return createdtime;
    }

    /**
     * 创建时间
     */
    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }

    /**
     * 修改时间
     */
    public Date getUpdatedtime() {
        return updatedtime;
    }

    /**
     * 修改时间
     */
    public void setUpdatedtime(Date updatedtime) {
        this.updatedtime = updatedtime;
    }

    /**
     * 修改人id
     */
    public Long getUpdateduserid() {
        return updateduserid;
    }

    /**
     * 修改人id
     */
    public void setUpdateduserid(Long updateduserid) {
        this.updateduserid = updateduserid;
    }

    /**
     * 企业营业执照的存储路径
     */
    public String getCompanylicpicpath() {
        return companylicpicpath;
    }

    /**
     * 企业营业执照的存储路径
     */
    public void setCompanylicpicpath(String companylicpicpath) {
        this.companylicpicpath = companylicpicpath;
    }

    /**
     * 组织机构代码证的存储路径
     */
    public String getOrgcodepicpath() {
        return orgcodepicpath;
    }

    /**
     * 组织机构代码证的存储路径
     */
    public void setOrgcodepicpath(String orgcodepicpath) {
        this.orgcodepicpath = orgcodepicpath;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Supplier other = (Supplier) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getSupcode() == null ? other.getSupcode() == null : this.getSupcode().equals(other.getSupcode()))
            && (this.getSupname() == null ? other.getSupname() == null : this.getSupname().equals(other.getSupname()))
            && (this.getSupdesc() == null ? other.getSupdesc() == null : this.getSupdesc().equals(other.getSupdesc()))
            && (this.getSupcontact() == null ? other.getSupcontact() == null : this.getSupcontact().equals(other.getSupcontact()))
            && (this.getSupphone() == null ? other.getSupphone() == null : this.getSupphone().equals(other.getSupphone()))
            && (this.getSupaddress() == null ? other.getSupaddress() == null : this.getSupaddress().equals(other.getSupaddress()))
            && (this.getSupfax() == null ? other.getSupfax() == null : this.getSupfax().equals(other.getSupfax()))
            && (this.getCreateduserid() == null ? other.getCreateduserid() == null : this.getCreateduserid().equals(other.getCreateduserid()))
            && (this.getCreatedtime() == null ? other.getCreatedtime() == null : this.getCreatedtime().equals(other.getCreatedtime()))
            && (this.getUpdatedtime() == null ? other.getUpdatedtime() == null : this.getUpdatedtime().equals(other.getUpdatedtime()))
            && (this.getUpdateduserid() == null ? other.getUpdateduserid() == null : this.getUpdateduserid().equals(other.getUpdateduserid()))
            && (this.getCompanylicpicpath() == null ? other.getCompanylicpicpath() == null : this.getCompanylicpicpath().equals(other.getCompanylicpicpath()))
            && (this.getOrgcodepicpath() == null ? other.getOrgcodepicpath() == null : this.getOrgcodepicpath().equals(other.getOrgcodepicpath()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getSupcode() == null) ? 0 : getSupcode().hashCode());
        result = prime * result + ((getSupname() == null) ? 0 : getSupname().hashCode());
        result = prime * result + ((getSupdesc() == null) ? 0 : getSupdesc().hashCode());
        result = prime * result + ((getSupcontact() == null) ? 0 : getSupcontact().hashCode());
        result = prime * result + ((getSupphone() == null) ? 0 : getSupphone().hashCode());
        result = prime * result + ((getSupaddress() == null) ? 0 : getSupaddress().hashCode());
        result = prime * result + ((getSupfax() == null) ? 0 : getSupfax().hashCode());
        result = prime * result + ((getCreateduserid() == null) ? 0 : getCreateduserid().hashCode());
        result = prime * result + ((getCreatedtime() == null) ? 0 : getCreatedtime().hashCode());
        result = prime * result + ((getUpdatedtime() == null) ? 0 : getUpdatedtime().hashCode());
        result = prime * result + ((getUpdateduserid() == null) ? 0 : getUpdateduserid().hashCode());
        result = prime * result + ((getCompanylicpicpath() == null) ? 0 : getCompanylicpicpath().hashCode());
        result = prime * result + ((getOrgcodepicpath() == null) ? 0 : getOrgcodepicpath().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", supcode=").append(supcode);
        sb.append(", supname=").append(supname);
        sb.append(", supdesc=").append(supdesc);
        sb.append(", supcontact=").append(supcontact);
        sb.append(", supphone=").append(supphone);
        sb.append(", supaddress=").append(supaddress);
        sb.append(", supfax=").append(supfax);
        sb.append(", createduserid=").append(createduserid);
        sb.append(", createdtime=").append(createdtime);
        sb.append(", updatedtime=").append(updatedtime);
        sb.append(", updateduserid=").append(updateduserid);
        sb.append(", companylicpicpath=").append(companylicpicpath);
        sb.append(", orgcodepicpath=").append(orgcodepicpath);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}