package ch10.demo.com.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统角色
 * @TableName t_sys_role
 */
public class SysRole implements Serializable {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 角色编码
     */
    private String code;

    /**
     * 角色名称
     */
    private String rolename;

    /**
     * 创建者
     */
    private Long createduserid;

    /**
     * 创建时间
     */
    private Date createdtime;

    /**
     * 修改者
     */
    private Long updateduserid;

    /**
     * 修改时间
     */
    private Date updatedtime;

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 角色编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 角色编码
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 角色名称
     */
    public String getRolename() {
        return rolename;
    }

    /**
     * 角色名称
     */
    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    /**
     * 创建者
     */
    public Long getCreateduserid() {
        return createduserid;
    }

    /**
     * 创建者
     */
    public void setCreateduserid(Long createduserid) {
        this.createduserid = createduserid;
    }

    /**
     * 创建时间
     */
    public Date getCreatedtime() {
        return createdtime;
    }

    /**
     * 创建时间
     */
    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }

    /**
     * 修改者
     */
    public Long getUpdateduserid() {
        return updateduserid;
    }

    /**
     * 修改者
     */
    public void setUpdateduserid(Long updateduserid) {
        this.updateduserid = updateduserid;
    }

    /**
     * 修改时间
     */
    public Date getUpdatedtime() {
        return updatedtime;
    }

    /**
     * 修改时间
     */
    public void setUpdatedtime(Date updatedtime) {
        this.updatedtime = updatedtime;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SysRole other = (SysRole) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getCode() == null ? other.getCode() == null : this.getCode().equals(other.getCode()))
            && (this.getRolename() == null ? other.getRolename() == null : this.getRolename().equals(other.getRolename()))
            && (this.getCreateduserid() == null ? other.getCreateduserid() == null : this.getCreateduserid().equals(other.getCreateduserid()))
            && (this.getCreatedtime() == null ? other.getCreatedtime() == null : this.getCreatedtime().equals(other.getCreatedtime()))
            && (this.getUpdateduserid() == null ? other.getUpdateduserid() == null : this.getUpdateduserid().equals(other.getUpdateduserid()))
            && (this.getUpdatedtime() == null ? other.getUpdatedtime() == null : this.getUpdatedtime().equals(other.getUpdatedtime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
        result = prime * result + ((getRolename() == null) ? 0 : getRolename().hashCode());
        result = prime * result + ((getCreateduserid() == null) ? 0 : getCreateduserid().hashCode());
        result = prime * result + ((getCreatedtime() == null) ? 0 : getCreatedtime().hashCode());
        result = prime * result + ((getUpdateduserid() == null) ? 0 : getUpdateduserid().hashCode());
        result = prime * result + ((getUpdatedtime() == null) ? 0 : getUpdatedtime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", code=").append(code);
        sb.append(", rolename=").append(rolename);
        sb.append(", createduserid=").append(createduserid);
        sb.append(", createdtime=").append(createdtime);
        sb.append(", updateduserid=").append(updateduserid);
        sb.append(", updatedtime=").append(updatedtime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}