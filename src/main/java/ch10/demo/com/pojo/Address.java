package ch10.demo.com.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @TableName t_address
 */
public class Address implements Serializable {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 联系人姓名
     */
    private String contact;

    /**
     * 收货地址明细
     */
    private String addressdesc;

    /**
     * 邮编
     */
    private String postcode;

    /**
     * 联系人电话
     */
    private String tel;

    /**
     * 创建者
     */
    private Long createduserid;

    /**
     * 创建时间
     */
    private Date createdtime;

    /**
     * 修改者
     */
    private Long updateduserid;

    /**
     * 修改时间
     */
    private Date updatedtime;

    /**
     * 用户ID
     */
    private Long userid;

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 联系人姓名
     */
    public String getContact() {
        return contact;
    }

    /**
     * 联系人姓名
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * 收货地址明细
     */
    public String getAddressdesc() {
        return addressdesc;
    }

    /**
     * 收货地址明细
     */
    public void setAddressdesc(String addressdesc) {
        this.addressdesc = addressdesc;
    }

    /**
     * 邮编
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * 邮编
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    /**
     * 联系人电话
     */
    public String getTel() {
        return tel;
    }

    /**
     * 联系人电话
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * 创建者
     */
    public Long getCreateduserid() {
        return createduserid;
    }

    /**
     * 创建者
     */
    public void setCreateduserid(Long createduserid) {
        this.createduserid = createduserid;
    }

    /**
     * 创建时间
     */
    public Date getCreatedtime() {
        return createdtime;
    }

    /**
     * 创建时间
     */
    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }

    /**
     * 修改者
     */
    public Long getUpdateduserid() {
        return updateduserid;
    }

    /**
     * 修改者
     */
    public void setUpdateduserid(Long updateduserid) {
        this.updateduserid = updateduserid;
    }

    /**
     * 修改时间
     */
    public Date getUpdatedtime() {
        return updatedtime;
    }

    /**
     * 修改时间
     */
    public void setUpdatedtime(Date updatedtime) {
        this.updatedtime = updatedtime;
    }

    /**
     * 用户ID
     */
    public Long getUserid() {
        return userid;
    }

    /**
     * 用户ID
     */
    public void setUserid(Long userid) {
        this.userid = userid;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Address other = (Address) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getContact() == null ? other.getContact() == null : this.getContact().equals(other.getContact()))
            && (this.getAddressdesc() == null ? other.getAddressdesc() == null : this.getAddressdesc().equals(other.getAddressdesc()))
            && (this.getPostcode() == null ? other.getPostcode() == null : this.getPostcode().equals(other.getPostcode()))
            && (this.getTel() == null ? other.getTel() == null : this.getTel().equals(other.getTel()))
            && (this.getCreateduserid() == null ? other.getCreateduserid() == null : this.getCreateduserid().equals(other.getCreateduserid()))
            && (this.getCreatedtime() == null ? other.getCreatedtime() == null : this.getCreatedtime().equals(other.getCreatedtime()))
            && (this.getUpdateduserid() == null ? other.getUpdateduserid() == null : this.getUpdateduserid().equals(other.getUpdateduserid()))
            && (this.getUpdatedtime() == null ? other.getUpdatedtime() == null : this.getUpdatedtime().equals(other.getUpdatedtime()))
            && (this.getUserid() == null ? other.getUserid() == null : this.getUserid().equals(other.getUserid()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getContact() == null) ? 0 : getContact().hashCode());
        result = prime * result + ((getAddressdesc() == null) ? 0 : getAddressdesc().hashCode());
        result = prime * result + ((getPostcode() == null) ? 0 : getPostcode().hashCode());
        result = prime * result + ((getTel() == null) ? 0 : getTel().hashCode());
        result = prime * result + ((getCreateduserid() == null) ? 0 : getCreateduserid().hashCode());
        result = prime * result + ((getCreatedtime() == null) ? 0 : getCreatedtime().hashCode());
        result = prime * result + ((getUpdateduserid() == null) ? 0 : getUpdateduserid().hashCode());
        result = prime * result + ((getUpdatedtime() == null) ? 0 : getUpdatedtime().hashCode());
        result = prime * result + ((getUserid() == null) ? 0 : getUserid().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", contact=").append(contact);
        sb.append(", addressdesc=").append(addressdesc);
        sb.append(", postcode=").append(postcode);
        sb.append(", tel=").append(tel);
        sb.append(", createduserid=").append(createduserid);
        sb.append(", createdtime=").append(createdtime);
        sb.append(", updateduserid=").append(updateduserid);
        sb.append(", updatedtime=").append(updatedtime);
        sb.append(", userid=").append(userid);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}