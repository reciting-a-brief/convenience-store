package ch10.demo.com.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 入库记录
 * @TableName t_storage_record
 */
public class StorageRecord implements Serializable {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 入库记录编码
     */
    private String srcode;

    /**
     * 商品名称
     */
    private String goodsname;

    /**
     * 商品描述
     */
    private String goodsdesc;

    /**
     * 商品单位
     */
    private String goodsunit;

    /**
     * 入库数量
     */
    private BigDecimal goodscount;

    /**
     * 入库商品总额
     */
    private BigDecimal totalamount;

    /**
     * 支付状态（1：未支付 2：已支付）
     */
    private Integer paystatus;

    /**
     * 创建人id
     */
    private Long createduserid;

    /**
     * 创建时间
     */
    private Date createdtime;

    /**
     * 修改人id
     */
    private Long updateduserid;

    /**
     * 修改时间
     */
    private Date updatedtime;

    /**
     * 供货商ID
     */
    private Long supplierid;

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 入库记录编码
     */
    public String getSrcode() {
        return srcode;
    }

    /**
     * 入库记录编码
     */
    public void setSrcode(String srcode) {
        this.srcode = srcode;
    }

    /**
     * 商品名称
     */
    public String getGoodsname() {
        return goodsname;
    }

    /**
     * 商品名称
     */
    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    /**
     * 商品描述
     */
    public String getGoodsdesc() {
        return goodsdesc;
    }

    /**
     * 商品描述
     */
    public void setGoodsdesc(String goodsdesc) {
        this.goodsdesc = goodsdesc;
    }

    /**
     * 商品单位
     */
    public String getGoodsunit() {
        return goodsunit;
    }

    /**
     * 商品单位
     */
    public void setGoodsunit(String goodsunit) {
        this.goodsunit = goodsunit;
    }

    /**
     * 入库数量
     */
    public BigDecimal getGoodscount() {
        return goodscount;
    }

    /**
     * 入库数量
     */
    public void setGoodscount(BigDecimal goodscount) {
        this.goodscount = goodscount;
    }

    /**
     * 入库商品总额
     */
    public BigDecimal getTotalamount() {
        return totalamount;
    }

    /**
     * 入库商品总额
     */
    public void setTotalamount(BigDecimal totalamount) {
        this.totalamount = totalamount;
    }

    /**
     * 支付状态（1：未支付 2：已支付）
     */
    public Integer getPaystatus() {
        return paystatus;
    }

    /**
     * 支付状态（1：未支付 2：已支付）
     */
    public void setPaystatus(Integer paystatus) {
        this.paystatus = paystatus;
    }

    /**
     * 创建人id
     */
    public Long getCreateduserid() {
        return createduserid;
    }

    /**
     * 创建人id
     */
    public void setCreateduserid(Long createduserid) {
        this.createduserid = createduserid;
    }

    /**
     * 创建时间
     */
    public Date getCreatedtime() {
        return createdtime;
    }

    /**
     * 创建时间
     */
    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }

    /**
     * 修改人id
     */
    public Long getUpdateduserid() {
        return updateduserid;
    }

    /**
     * 修改人id
     */
    public void setUpdateduserid(Long updateduserid) {
        this.updateduserid = updateduserid;
    }

    /**
     * 修改时间
     */
    public Date getUpdatedtime() {
        return updatedtime;
    }

    /**
     * 修改时间
     */
    public void setUpdatedtime(Date updatedtime) {
        this.updatedtime = updatedtime;
    }

    /**
     * 供货商ID
     */
    public Long getSupplierid() {
        return supplierid;
    }

    /**
     * 供货商ID
     */
    public void setSupplierid(Long supplierid) {
        this.supplierid = supplierid;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        StorageRecord other = (StorageRecord) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getSrcode() == null ? other.getSrcode() == null : this.getSrcode().equals(other.getSrcode()))
            && (this.getGoodsname() == null ? other.getGoodsname() == null : this.getGoodsname().equals(other.getGoodsname()))
            && (this.getGoodsdesc() == null ? other.getGoodsdesc() == null : this.getGoodsdesc().equals(other.getGoodsdesc()))
            && (this.getGoodsunit() == null ? other.getGoodsunit() == null : this.getGoodsunit().equals(other.getGoodsunit()))
            && (this.getGoodscount() == null ? other.getGoodscount() == null : this.getGoodscount().equals(other.getGoodscount()))
            && (this.getTotalamount() == null ? other.getTotalamount() == null : this.getTotalamount().equals(other.getTotalamount()))
            && (this.getPaystatus() == null ? other.getPaystatus() == null : this.getPaystatus().equals(other.getPaystatus()))
            && (this.getCreateduserid() == null ? other.getCreateduserid() == null : this.getCreateduserid().equals(other.getCreateduserid()))
            && (this.getCreatedtime() == null ? other.getCreatedtime() == null : this.getCreatedtime().equals(other.getCreatedtime()))
            && (this.getUpdateduserid() == null ? other.getUpdateduserid() == null : this.getUpdateduserid().equals(other.getUpdateduserid()))
            && (this.getUpdatedtime() == null ? other.getUpdatedtime() == null : this.getUpdatedtime().equals(other.getUpdatedtime()))
            && (this.getSupplierid() == null ? other.getSupplierid() == null : this.getSupplierid().equals(other.getSupplierid()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getSrcode() == null) ? 0 : getSrcode().hashCode());
        result = prime * result + ((getGoodsname() == null) ? 0 : getGoodsname().hashCode());
        result = prime * result + ((getGoodsdesc() == null) ? 0 : getGoodsdesc().hashCode());
        result = prime * result + ((getGoodsunit() == null) ? 0 : getGoodsunit().hashCode());
        result = prime * result + ((getGoodscount() == null) ? 0 : getGoodscount().hashCode());
        result = prime * result + ((getTotalamount() == null) ? 0 : getTotalamount().hashCode());
        result = prime * result + ((getPaystatus() == null) ? 0 : getPaystatus().hashCode());
        result = prime * result + ((getCreateduserid() == null) ? 0 : getCreateduserid().hashCode());
        result = prime * result + ((getCreatedtime() == null) ? 0 : getCreatedtime().hashCode());
        result = prime * result + ((getUpdateduserid() == null) ? 0 : getUpdateduserid().hashCode());
        result = prime * result + ((getUpdatedtime() == null) ? 0 : getUpdatedtime().hashCode());
        result = prime * result + ((getSupplierid() == null) ? 0 : getSupplierid().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", srcode=").append(srcode);
        sb.append(", goodsname=").append(goodsname);
        sb.append(", goodsdesc=").append(goodsdesc);
        sb.append(", goodsunit=").append(goodsunit);
        sb.append(", goodscount=").append(goodscount);
        sb.append(", totalamount=").append(totalamount);
        sb.append(", paystatus=").append(paystatus);
        sb.append(", createduserid=").append(createduserid);
        sb.append(", createdtime=").append(createdtime);
        sb.append(", updateduserid=").append(updateduserid);
        sb.append(", updatedtime=").append(updatedtime);
        sb.append(", supplierid=").append(supplierid);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}