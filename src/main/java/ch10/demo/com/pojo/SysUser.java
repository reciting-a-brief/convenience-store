package ch10.demo.com.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统用户
 * @TableName t_sys_user
 */
public class SysUser implements Serializable {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 账号
     */
    private String account;

    /**
     * 真是姓名
     */
    private String realname;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别（1:女、 2:男）
     */
    private Integer sex;

    /**
     * 出生日期（年-月-日）
     */
    private Date birthday;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 用户地址
     */
    private String address;

    /**
     * 用户角色id
     */
    private Long roleid;

    /**
     * 创建人
     */
    private Long createduserid;

    /**
     * 创建时间
     */
    private Date createdtime;

    /**
     * 修改人
     */
    private Long updateduserid;

    /**
     * 修改时间
     */
    private Date updatedtime;

    /**
     * 证件照路径
     */
    private String idpicpath;

    /**
     * 工作证照片路径
     */
    private String workpicpath;

    private static final long serialVersionUID = 1L;

    public SysUser(Long id, String account, String realname, String password, int
            sex, String phone, String address, Long roleid,
                   Long createduserid, Date createdtime, Long updateduserid, Date updatedtime) {
        super();
        this.id = id;
        this.account = account;
        this.realname = realname;
        this.password = password;
        this.sex = sex;
        this.phone = phone;
        this.address = address;
        this.roleid = roleid;
        this.createduserid = createduserid;
        this.createdtime = createdtime;
        this.updateduserid = updateduserid;
        this.updatedtime = updatedtime;
    }


    /**
     * 主键ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 账号
     */
    public String getAccount() {
        return account;
    }

    /**
     * 账号
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * 真是姓名
     */
    public String getRealname() {
        return realname;
    }

    /**
     * 真是姓名
     */
    public void setRealname(String realname) {
        this.realname = realname;
    }

    /**
     * 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 性别（1:女、 2:男）
     */
    public Integer getSex() {
        return sex;
    }

    /**
     * 性别（1:女、 2:男）
     */
    public void setSex(Integer sex) {
        this.sex = sex;
    }

    /**
     * 出生日期（年-月-日）
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * 出生日期（年-月-日）
     */
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    /**
     * 手机号码
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 手机号码
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 用户地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 用户地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 用户角色id
     */
    public Long getRoleid() {
        return roleid;
    }

    /**
     * 用户角色id
     */
    public void setRoleid(Long roleid) {
        this.roleid = roleid;
    }

    /**
     * 创建人
     */
    public Long getCreateduserid() {
        return createduserid;
    }

    /**
     * 创建人
     */
    public void setCreateduserid(Long createduserid) {
        this.createduserid = createduserid;
    }

    /**
     * 创建时间
     */
    public Date getCreatedtime() {
        return createdtime;
    }

    /**
     * 创建时间
     */
    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }

    /**
     * 修改人
     */
    public Long getUpdateduserid() {
        return updateduserid;
    }

    /**
     * 修改人
     */
    public void setUpdateduserid(Long updateduserid) {
        this.updateduserid = updateduserid;
    }

    /**
     * 修改时间
     */
    public Date getUpdatedtime() {
        return updatedtime;
    }

    /**
     * 修改时间
     */
    public void setUpdatedtime(Date updatedtime) {
        this.updatedtime = updatedtime;
    }

    /**
     * 证件照路径
     */
    public String getIdpicpath() {
        return idpicpath;
    }

    /**
     * 证件照路径
     */
    public void setIdpicpath(String idpicpath) {
        this.idpicpath = idpicpath;
    }

    /**
     * 工作证照片路径
     */
    public String getWorkpicpath() {
        return workpicpath;
    }

    /**
     * 工作证照片路径
     */
    public void setWorkpicpath(String workpicpath) {
        this.workpicpath = workpicpath;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SysUser other = (SysUser) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getAccount() == null ? other.getAccount() == null : this.getAccount().equals(other.getAccount()))
            && (this.getRealname() == null ? other.getRealname() == null : this.getRealname().equals(other.getRealname()))
            && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
            && (this.getSex() == null ? other.getSex() == null : this.getSex().equals(other.getSex()))
            && (this.getBirthday() == null ? other.getBirthday() == null : this.getBirthday().equals(other.getBirthday()))
            && (this.getPhone() == null ? other.getPhone() == null : this.getPhone().equals(other.getPhone()))
            && (this.getAddress() == null ? other.getAddress() == null : this.getAddress().equals(other.getAddress()))
            && (this.getRoleid() == null ? other.getRoleid() == null : this.getRoleid().equals(other.getRoleid()))
            && (this.getCreateduserid() == null ? other.getCreateduserid() == null : this.getCreateduserid().equals(other.getCreateduserid()))
            && (this.getCreatedtime() == null ? other.getCreatedtime() == null : this.getCreatedtime().equals(other.getCreatedtime()))
            && (this.getUpdateduserid() == null ? other.getUpdateduserid() == null : this.getUpdateduserid().equals(other.getUpdateduserid()))
            && (this.getUpdatedtime() == null ? other.getUpdatedtime() == null : this.getUpdatedtime().equals(other.getUpdatedtime()))
            && (this.getIdpicpath() == null ? other.getIdpicpath() == null : this.getIdpicpath().equals(other.getIdpicpath()))
            && (this.getWorkpicpath() == null ? other.getWorkpicpath() == null : this.getWorkpicpath().equals(other.getWorkpicpath()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getAccount() == null) ? 0 : getAccount().hashCode());
        result = prime * result + ((getRealname() == null) ? 0 : getRealname().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getSex() == null) ? 0 : getSex().hashCode());
        result = prime * result + ((getBirthday() == null) ? 0 : getBirthday().hashCode());
        result = prime * result + ((getPhone() == null) ? 0 : getPhone().hashCode());
        result = prime * result + ((getAddress() == null) ? 0 : getAddress().hashCode());
        result = prime * result + ((getRoleid() == null) ? 0 : getRoleid().hashCode());
        result = prime * result + ((getCreateduserid() == null) ? 0 : getCreateduserid().hashCode());
        result = prime * result + ((getCreatedtime() == null) ? 0 : getCreatedtime().hashCode());
        result = prime * result + ((getUpdateduserid() == null) ? 0 : getUpdateduserid().hashCode());
        result = prime * result + ((getUpdatedtime() == null) ? 0 : getUpdatedtime().hashCode());
        result = prime * result + ((getIdpicpath() == null) ? 0 : getIdpicpath().hashCode());
        result = prime * result + ((getWorkpicpath() == null) ? 0 : getWorkpicpath().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", account=").append(account);
        sb.append(", realname=").append(realname);
        sb.append(", password=").append(password);
        sb.append(", sex=").append(sex);
        sb.append(", birthday=").append(birthday);
        sb.append(", phone=").append(phone);
        sb.append(", address=").append(address);
        sb.append(", roleid=").append(roleid);
        sb.append(", createduserid=").append(createduserid);
        sb.append(", createdtime=").append(createdtime);
        sb.append(", updateduserid=").append(updateduserid);
        sb.append(", updatedtime=").append(updatedtime);
        sb.append(", idpicpath=").append(idpicpath);
        sb.append(", workpicpath=").append(workpicpath);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}