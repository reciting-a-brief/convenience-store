package ch10.demo.com.dao.mapper;

import ch10.demo.com.pojo.Supplier;

/**
* @author Lenovo
* @description 针对表【t_supplier(药品供货商)】的数据库操作Mapper
* @createDate 2024-04-23 11:31:49
* @Entity ch10.demo.com.pojo.Supplier
*/
public interface SupplierMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Supplier record);

    int insertSelective(Supplier record);

    Supplier selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Supplier record);

    int updateByPrimaryKey(Supplier record);

}
