package ch10.demo.com.dao.mapper;

import ch10.demo.com.pojo.SysRole;

/**
* @author Lenovo
* @description 针对表【t_sys_role(系统角色)】的数据库操作Mapper
* @createDate 2024-04-23 11:31:49
* @Entity ch10.demo.com.pojo.SysRole
*/
public interface SysRoleMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);

}
