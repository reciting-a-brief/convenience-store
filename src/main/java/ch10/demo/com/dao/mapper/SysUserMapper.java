package ch10.demo.com.dao.mapper;

import ch10.demo.com.pojo.SysUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
* @author Lenovo
* @description 针对表【t_sys_user(系统用户)】的数据库操作Mapper
* @createDate 2024-04-23 11:31:49
* @Entity ch10.demo.com.pojo.SysUser
*/
@Repository
public interface SysUserMapper {
    /**
     * 根据账号和密码进行查询，即实现登录的功能
     * @param account 账号
     * @param password 秘密
     * @return 查询到的用户信息
     */
    SysUser queryByAccountAndPwd(@Param("account") String account,@Param("password") String password);

    int deleteByPrimaryKey(Long id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

}
