package ch10.demo.com.dao.mapper;

import ch10.demo.com.pojo.StorageRecord;

/**
* @author Lenovo
* @description 针对表【t_storage_record(入库记录)】的数据库操作Mapper
* @createDate 2024-04-23 11:31:49
* @Entity ch10.demo.com.pojo.StorageRecord
*/
public interface StorageRecordMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StorageRecord record);

    int insertSelective(StorageRecord record);

    StorageRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StorageRecord record);

    int updateByPrimaryKey(StorageRecord record);

}
