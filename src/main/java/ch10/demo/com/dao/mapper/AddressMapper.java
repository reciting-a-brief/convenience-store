package ch10.demo.com.dao.mapper;

import ch10.demo.com.pojo.Address;

/**
* @author Lenovo
* @description 针对表【t_address】的数据库操作Mapper
* @createDate 2024-04-23 11:31:49
* @Entity ch10.demo.com.pojo.Address
*/
public interface AddressMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Address record);

    int insertSelective(Address record);

    Address selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Address record);

    int updateByPrimaryKey(Address record);

}
