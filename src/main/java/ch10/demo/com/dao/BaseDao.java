package ch10.demo.com.dao;

import ch10.demo.com.utils.ConfigManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @Description: TODO
 * @author: 周杰
 * @date: 2024年04月23日9:36
 */
public class BaseDao {
    /**
     * 返回数据数据库连接
     */
    public static Connection getConnection(){
        Connection conn = null;
        String driver = ConfigManager.getInstance().getValue("driver");
        String url = ConfigManager.getInstance().getValue("url");
        String username = ConfigManager.getInstance().getValue("username");
        String password = ConfigManager.getInstance().getValue("password");
        try{
            Class.forName(driver);
            conn = DriverManager.getConnection(url,username,password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return conn;
    }
}
