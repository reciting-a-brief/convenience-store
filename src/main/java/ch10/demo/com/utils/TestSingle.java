package ch10.demo.com.utils;

/**
 * @Description: TODO
 * @author: 周杰
 * @date: 2024年04月23日11:24
 */
public class TestSingle {
    public static void main(String[] args) {
        Single single = Single.test();
        System.out.println("调用Singleton.test()方法后，返回的实例是：" + single);
        single = Single.getInstance();
        System.out.println("调用Singleton.getInstance()方法后，返回的实例是：" + single);
        single = Single.getInstance();
        System.out.println("再次调用Singleton.getInstance()方法后，返回的实例是：" + single);
    }

}
