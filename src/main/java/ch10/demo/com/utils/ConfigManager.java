package ch10.demo.com.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @Description: TODO
 * @author: 周杰
 * @date: 2024年04月23日8:43
 */
//通过单例模式读取配置文件
public class ConfigManager {
    private static Properties properties;
    private static ConfigManager configManager = new ConfigManager();
    //杜绝了外界创建该类对象，私有构造方法。
    private ConfigManager(){
        String configFile = "database.properties";
        properties = new Properties();
        //得到当前类的类加载器并将配置文件加载到输入流中
        InputStream is = ConfigManager.class.getClassLoader().getResourceAsStream(configFile);
        try{
            //从输入流中读取属性属性列表
            properties.load(is);
            is.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    //饿汉模式
    /**
     * 对外提供的获取ConfigManager实例的方法
     */
    public synchronized static ConfigManager getInstance(){
        return configManager;
    }
    public String getValue(String key){
        return properties.getProperty(key);
    }
}
