package ch10.demo.com.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @Description: TODO
 * @author: 周杰
 * @date: 2024年04月23日11:06
 */
public class Single {
    private static Single single;
    private static Properties properties;
    //私有构造方法，项目运行期间，仅执行一次
    private Single(){
        String configFile = "database.properties";
        properties = new Properties();
        //得到当前类的类加载器并将配置文件加载到输入流中
        InputStream is = Single.class.getClassLoader().getResourceAsStream(configFile);
        try{
            //从输入流中读取属性列表
            properties.load(is);
            is.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    //定义一个静态内部类，是将创建工作交给静态内部类。
    public static class SingleHelper{
        private static final Single INSTANCE = new Single();
    }
    //全局访问点
    public static Single getInstance(){
        single = SingleHelper.INSTANCE;
        return single;
    }
    public String getVale(String key){
        return properties.getProperty(key);
    }
    public static Single test(){
        return single;
    }
}
